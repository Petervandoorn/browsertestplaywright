from playwright.sync_api import sync_playwright
#refer to: https://playwright.dev/python/docs/why-playwright to get more information on PlayWright
#Written by Peter van Doorn on 2021-07-15
import time
import uuid
import sys, os


class bcolors:
    OK = '\033[92m' #GREEN
    WARNING = '\033[93m' #YELLOW
    FAIL = '\033[91m' #RED
    RESET = '\033[0m' #RESET COLOR



while True:

    print(bcolors.OK+"\n"+"\n"+"\n"+"#####################"+bcolors.RESET)
    print(bcolors.OK+"### Starting test ###"+bcolors.RESET)
    print(bcolors.OK+"#####################"+bcolors.RESET)
    print("\n"+"\n"+"-- Setting Test Parameters")

    # Setting variables

    # Setting location
    location = input("\n"+"\n"+"What location do you want to use for testing? If left empty nl-NL will be used"+"\n"+bcolors.RESET)
    if location:
        print(bcolors.OK+ "-- Location set to: "+location + bcolors.RESET)
    else:
        print("-- Standard location set nl-NL")
        location = "nl-NL"

    # Setting screen sizes
    width = input("\n"+"\n"+"What width browser size do you want to use for testing? If left empty width is set to 1920 pixels"+"\n" +bcolors.RESET)
    if width:
        print(bcolors.OK+ "-- Width set to: "+width+"pixels" +bcolors.RESET)
        width = int(width)
    else:
        print("-- Width  set to 1920px")
        width = 1920

    height = input("\n"+"\n"+"What height browser size do you want to use for testing? If left empty height is set to 1080 pixels"+"\n")
    if height:
        print(bcolors.OK+ "-- Height set to: "+height+"pixels"+bcolors.RESET)
        height = int(height)
    else:
        print("-- Height  set to 1080")
        height = 1080

    timezone = input("\n"+"\n"+"What timezone do you want to use for testing? If left empty height is set to Europe/Amsterdam"+"\n")
    if timezone:
        print(bcolors.OK+ "-- Timezone set to: "+timezone+ bcolors.RESET)
    else:
        print("-- Timezone  set to Europe/Amsterdam")
        timezone = "Europe/Amsterdam"

    slowdown = input("\n"+"\n"+"At what speed do you want the script to test? If left empty a value of 1500 is used"+"\n"+"n.b. higher = slower"+"\n")
    if slowdown:
        print(bcolors.OK+ "-- Speed set to: "+slowdown+bcolors.RESET)
        slowdown = int(slowdown)
    else:
        print("-- Speed  set to 1500")
        slowdown = 1500

    randomname = uuid.uuid1()
    randomname = str(randomname)






    # Start of main testing script

    def run(playwright):  
        browserselect = input("\n"+"\n"+"What browser do you want to use"+"\n"+"- 1. For chrome type 1"+"\n"+"- 2. For Safari type 2"+"\n"+"- 3. For Firefox type 3"+"\n"+"- 4. For Edge type 4"+"\n"+"\n")

        if browserselect == '1':
            print(bcolors.OK+ "\n"+"\n"+"-- Starting test using chrome"+bcolors.RESET)
            browser = playwright.chromium.launch(headless=False, slow_mo=slowdown)
            browserused = "chrome"
        elif browserselect == '2':
            print(bcolors.OK+"\n"+"\n"+"-- Starting test using Safari"+bcolors.RESET)

            
            browser = playwright.webkit.launch(headless=False, slow_mo=slowdown)
            browserused = "Safari"
        elif browserselect == '3':
            print(bcolors.OK+"\n"+"\n"+"-- Starting test using Firefox"+bcolors.RESET) 
            browser = playwright.firefox.launch(headless=False, slow_mo=slowdown)  
            browserused = "firefox" 
        elif browserselect == '4':
            print(bcolors.OK+"\n"+"\n"+"-- Starting test using Edge"+bcolors.RESET)   
            browser = playwright.chromium.launch(headless=False, slow_mo=slowdown, channel="msedge")
            browserused = "Edge" 
        else:
            print(bcolors.FAIL +'Incorrect input'+bcolors.RESET)

        context = browser.new_context(
            viewport={ 'width': width, 'height': height },
            locale=location,
            timezone_id=timezone
        )

        # Start of test script
        print(bcolors.OK+"\n"+"\n"+"Testing started"+bcolors.RESET)
        print(bcolors.OK+"\n"+"\n"+"\n"+"#####################"+bcolors.RESET)
        print("screenshots in this session will be labeled with: "+bcolors.WARNING+browserused+"--"+randomname+bcolors.RESET+" as unique marker of this session in the folder screenshots")
        print("Loggin in")

           # Open new page
        page = context.new_page()

        # Go to https://www.wikipedia.org/
        page.goto("https://www.wikipedia.org/")

        # Click input[name="search"]
        page.click("input[name=\"search\"]")

        # Fill input[name="search"]
        page.fill("input[name=\"search\"]", "apple")

        # Press Enter
        page.press("input[name=\"search\"]", "Enter")
        # assert page.url == "https://en.wikipedia.org/wiki/Apple"
        page.screenshot(path="screenshots/screenshot-1--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        page.goto("https://www.wikipedia.org/wiki/Apple")

        # Click text="Greek mythology"
        page.click("text=\"Greek mythology\"")
        # assert page.url == "https://en.wikipedia.org/wiki/Apple#Greek_mythology"

        page.screenshot(path="screenshots/screenshot-2--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")
        # Click input[name="search"]
        page.click("input[name=\"search\"]")

        # Fill input[name="search"]
        page.fill("input[name=\"search\"]", "apple computers")

        # Press Enter
        # with page.expect_navigation(url="https://en.wikipedia.org/wiki/Apple_Inc."):
        with page.expect_navigation():
            page.press("input[name=\"search\"]", "Enter")
        page.screenshot(path="screenshots/screenshot-2--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        # Click //p[starts-with(normalize-space(.), 'At the August 1997 Macworld Expo in Boston, Jobs announced that Apple would join')]
        page.click("//p[starts-with(normalize-space(.), 'At the August 1997 Macworld Expo in Boston, Jobs announced that Apple would join')]")

        page.screenshot(path="screenshots/screenshot-3--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")
        # Press f with modifiers
        page.press("//body[starts-with(normalize-space(.), 'Apple Inc. From Wikipedia, the free encyclopedia   (Redirected from Apple comput')]", "Meta+f")

        # Press f with modifiers
        page.press("//body[starts-with(normalize-space(.), 'Apple Inc. From Wikipedia, the free encyclopedia   (Redirected from Apple comput')]", "Meta+f")

        # Press f with modifiers
        page.press("//body[starts-with(normalize-space(.), 'Apple Inc. From Wikipedia, the free encyclopedia   (Redirected from Apple comput')]", "Meta+f")

        # Click text="Microsoft Office"
        page.click("text=\"Microsoft Office\"")
        # assert page.url == "https://en.wikipedia.org/wiki/Microsoft_Office"
        page.screenshot(path="screenshots/screenshot4--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        # Click //a[3][normalize-space(.)='Microsoft' and normalize-space(@title)='Microsoft']
        page.click("//a[3][normalize-space(.)='Microsoft' and normalize-space(@title)='Microsoft']")
        # assert page.url == "https://en.wikipedia.org/wiki/Microsoft"
        page.screenshot(path="screenshots/screenshot-5--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        # Click //a[2][normalize-space(.)='Bill Gates']
        page.click("//a[2][normalize-space(.)='Bill Gates']")
        # assert page.url == "https://en.wikipedia.org/wiki/Bill_Gates"
        page.screenshot(path="screenshots/screenshot-6--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        # Click text="Español"
        page.click("text=\"Español\"")
        # assert page.url == "https://es.wikipedia.org/wiki/Bill_Gates"
        page.screenshot(path="screenshots/screenshot-7--login--"+browserused+"--"+randomname+".png", full_page=True)
        print("Making a screenshot")

        # ---------------------
        context.close()
        browser.close()
    with sync_playwright() as playwright:
        run(playwright)
    while True:
        print(bcolors.WARNING+"\n"+"\n"+"Thanks for testing, do you want to run another test?")
        answer = str(input('(y/n): '))
        if answer in ('y', 'n'):
            break
        print(bcolors.FAIL+"invalid input."+bcolors.RESET)
    if answer == 'y':
        continue
    else:
        print(bcolors.OK+"Goodbye, and see you next time")
        break
