# Introduction
This is a simple script based on Playwright ( https://playwright.dev ). It allows you to program a fixed set of tasks to a browser and repeat it in several locales, screen sizes and browsers. I use it as a product owner to rapidly run through new versions of a web applications so a quick test can easily be performed. 

# what does the script do
It just runs playwright, but at the start it asks you for some variables: 
- Locale
- Language
- Timezone
- Screensize
- Browser

You can set these in Playwright, but I pass them as variables making it easier to use the script in real life. 
At the end of the script it asks you if you want to run it again, making multiple runs easy to do. 

I also made sure that the script gives a random name to a screenshot using this function: page.screenshot(path="screenshots/screenshot-3--login--"+browserused+"--"+randomname+".png", full_page=True) 


# How to use
First make sure Playwright is installed and all dependencies are met. Then run the following command in the terminal: 
$ playwright codegen website.you.want.to.test.com 

This will start a browser in which you can click around testing, meanwhile in the terminal window a script will be generated that can be repeated. 
You can paste your script in the script I uploaded here (double check indentation) between line 116 and 180. This will allow you to get a prompt that asks what browser to use and what resolution. 

# Known issues: 
Using this with microsoft edge is a pain in the ass, did not get that working yet. 
